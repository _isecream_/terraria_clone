extends Node2D

var block_variants_num = 7

var variant_pos = [" up", " in", " rot 0", " rot 1", " rot 2", " rot 3", " solo"]

var current_block = 0

var tiles = {
			 "air":-1,
			 "grass up":0,
			 "grass in":18,
			 "grass rot 0":1,
			 "grass rot 1":2,
			 "grass rot 2":3,
			 "grass rot 3":4,
			 "grass solo":11,
			
			 "dirt up":12,
			 "dirt in":5,
			 "dirt rot 0":13,
			 "dirt rot 1":14,
			 "dirt rot 2":15,
			 "dirt rot 3":19,
			 "dirt solo":20,
			
			 "rock up":6,
			 "rock in":16,
			 "rock rot 0":7,
			 "rock rot 1":8,
			 "rock rot 2":9,
			 "rock rot 3":10,
			 "rock solo":18,
			
			}
var blocks = ["grass", "dirt", "rock"]

var map = create_map(33, 32)

var deleting_block  = Vector2()


var mouse = Vector2()
var grass_delete_time = 1
var delete_time = 0
var isDeleting = false
var selected_tile = get_itae(tiles, "zemlia")


		
func _physics_process(delta):
	update_map()
	mouse = get_local_mouse_position()
	mouse.x = round(mouse.x)
	mouse.y = round(mouse.y)
	mouse /= 16
	$youwin.text = "x: "+ str(mouse.x) + " y: " + str(mouse.y) 
	$youwin3.text = str(1 / delta)
	
	
	
	if Input.is_action_pressed("put_block"):
		map[mouse.x][mouse.y] = get_block_var(current_block, 6)
		
	if Input.is_action_pressed("delete_block") and map[mouse.x][mouse.y] != "air":
		deleting_block.x = mouse.x
		deleting_block.y = mouse.y
		isDeleting = true
		
	pass
	
	if Input.is_action_just_released("delete_block"):
		isDeleting = false
		delete_time = 0
	pass
	
	if isDeleting:
		$youwin2.text = str(delete_time)
		delete_time += delta
		
		if mouse != deleting_block:
			isDeleting = false
			delete_time = 0
		
		if delete_time > grass_delete_time:
			isDeleting = false
			map[deleting_block.x][deleting_block.y] = "air"
			delete_time = 0
			
func get_itae(array, string):
	var j = 0
	
	for i in array:
		if i == string:
			return j
		j += 1
	pass

func create_map(w, h):
    var map = []

    for x in range(w):
        var col = []
        col.resize(h)
        map.append(col)

    return map

func _on_TileMap_ready():
	for x in range(33):
		for y in range(32):
			map[x][y] = "air"
			
	for i in range(33):
		map[i][28] = get_block_var(0, 0)
		map[i][29] = get_block_var(1, 1)
		map[i][30] = get_block_var(1, 1)
	
	
	update_map()
	
	
	
	pass # Replace with function body.

func update_map():
	
	for x in range(32):
		for y in range(31):
			
			if "grass" in map[x][y] : #grass
				if "grass" in map[x+1][y] and "grass" in map[x-1][y]:
					map[x][y] = get_block_var(0, 0)
	
	for x in range(33):
		for y in range(32):
			
			
			$TileMap.set_cell(x, y, tiles[map[x][y]]) 
			
	pass

func get_block_var(tile_id, variant_id):
	return blocks[tile_id] + variant_pos[variant_id]

#func check_block(num, wanted_num):
#	return (num / block_variants_num != wanted_num - 1) and  (num / block_variants_num != wanted_num + 1)